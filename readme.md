
### Case Design

![case-design](/crkbd-case.png)

### Keyboard Spec
- crkbd 3x6
- 0.5mm ixpe foam
- 3.5mm poron plate foam
- 1.5mm sandblasted pc plate
- azure dragon alike but slight lower pitched and discontinued switch filmed and lubed with 205g0
- taped mod 3 layers
- top case stack are mostly black pom, oled cover are clear acrylic and bottom sandblasted pc

![case-full-build-front-view](/crkbd-full-0.jpeg)

![case-full-build-top-view](/crkbd-full-1.jpeg)

### Testing
- video and audio record done using samsung s9+

![gasket-performance-test](/flex-test.mp4)

![typing-sound](/crkbd-full.wav)
